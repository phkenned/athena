# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration

# from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from TrkConfig.AtlasExtrapolatorConfig import AtlasExtrapolatorCfg
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.AccumulatorCache import AccumulatorCache

@AccumulatorCache
def AtlasTrackSummaryToolCfg(flags, name="AtlasTrackSummaryTool", **kwargs):
    # Based on AtlasTrackSummaryTool.py
    # FIXME - check all of this once the ID configuration is available, because probably we can simplify this a lot

    # Setup Atlas Extrapolator
    result = AtlasExtrapolatorCfg(flags)
    extrapolator = result.getPrimary()
    result.addPublicTool(extrapolator)

    # Setup Association Tool
    from InDetConfig.InDetAssociationToolsConfig import InDetPrdAssociationTool_noTRTCfg
    atlasPrdAssociationTool = result.popToolsAndMerge(InDetPrdAssociationTool_noTRTCfg(flags,
                                                                                       name='AtlasPrdAssociationTool'))
    result.addPublicTool(atlasPrdAssociationTool)

    # Loading Configurable HoleSearchTool
    from InDetConfig.InDetTrackHoleSearchConfig import InDetTrackHoleSearchToolCfg
    atlasHoleSearchTool = result.popToolsAndMerge(InDetTrackHoleSearchToolCfg(flags,
                                                                              name="AtlasHoleSearchTool",
                                                                              Extrapolator=extrapolator))
    result.addPublicTool(atlasHoleSearchTool)

    # FIXME - need InDet to provide configuration for PixelConditionsSummaryTool
    # Also assuming we don't use DetailedPixelHoleSearch (since it seems to be off in standard workflows)
    from InDetConfig.InDetTrackSummaryHelperToolConfig import InDetTrackSummaryHelperToolCfg
    indet_track_summary_helper_tool = result.popToolsAndMerge(InDetTrackSummaryHelperToolCfg(flags,
                                                                                             name="AtlasTrackSummaryHelperTool",
                                                                                             AssoTool=atlasPrdAssociationTool,
                                                                                             DoSharedHits=False,
                                                                                             HoleSearch=atlasHoleSearchTool))

    from MuonConfig.MuonRecToolsConfig import MuonTrackSummaryHelperToolCfg
    muon_track_summary_helper_tool = result.popToolsAndMerge(MuonTrackSummaryHelperToolCfg(flags))

    track_summary_tool = CompFactory.Trk.TrackSummaryTool(name=name,
                                                          doSharedHits=False,
                                                          doHolesInDet=True,
                                                          doHolesMuon=False,
                                                          AddDetailedMuonSummary=True,
                                                          InDetSummaryHelperTool=indet_track_summary_helper_tool,
                                                          MuonSummaryHelperTool=muon_track_summary_helper_tool,
                                                          PixelExists=True)
    result.setPrivateTools(track_summary_tool)
    return result
