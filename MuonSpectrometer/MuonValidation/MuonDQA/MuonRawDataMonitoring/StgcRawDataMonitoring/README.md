###### STG Raw Data monitoring algorithm for the OfflineDQ ######
Author: Philip David Kennedy
Created: September 2021
Based on: MM Raw Data monitoring algorthim

First STG OfflineDQ software was created by modification of the MM OfflineDQ software due to the similarities of the two detectors and the earlier creation of the MM software.

The following algorithm has required adjustment of data containers, detector geometry and various debugging. After compilation the algorithm can be run using:

cd python
python StgcMonitorAlgorithm.py

This will produce the file monitor.root which contains the selected overview histograms. In it's current form this wil give a residuals plot for the whole detector.

###### This module remains in active development ######
The latest updates can be found here:
https://gitlab.cern.ch/sfuenzal/athena/-/tree/master-sTGC_Develoment/MuonSpectrometer/MuonValidation/MuonDQA/MuonRawDataMonitoring/StgcRawDataMonitoring

## Algorithm development contact:
Sebastian Fuenzalida Garrido
sebastian.fuenzalida.garrido@cern.ch

## Post-processing development contact:
Georgios Lamprinoudis
georgios.lamprinoudis@cern.ch

## Technical supervisor:
Gerardo Vasquez
g.vasquez@cern.ch
