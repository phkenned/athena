/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Package : sTGCRawDataMonAlg
// Author: P. D. Kennedy
// MMAuthors:   M. Biglietti, E. Rossi (Roma Tre)
//
// DESCRIPTION:
// Subject: sTGC-->Offline Muon Data Quality
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonReadoutGeometry/MuonStation.h"
#include "MuonReadoutGeometry/sTgcReadoutElement.h"
#include "MuonDQAUtils/MuonChamberNameConverter.h"
#include "MuonDQAUtils/MuonChambersRange.h"
#include "MuonCalibIdentifier/MuonFixedId.h"

#include "StgcRawDataMonitoring/StgcRawDataMonAlg.h"

#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "MuonPrepRawData/MuonPrepDataContainer.h"
#include "MuonRIO_OnTrack/sTgcClusterOnTrack.h"
#include "AthenaMonitoring/AthenaMonManager.h"
#include "MuonPrepRawData/sTgcPrepData.h"

namespace {

  static constexpr double const toDeg = 180/M_PI;

  //1e=1.6X10-4 fC                                                                                          
  static constexpr double conversion_charge=1.6E-04;

  static const std::array<std::string,2> sTGC_Side = {"CSide", "ASide"};
  static const std::array<std::string,2> sTGC_Sector = {"S", "L"};
  static const std::array<std::string,2> EtaSector = {"1","2"};


  struct sTGCOverviewHistogramStruct {

    std::vector<int> statEta_strip;
    std::vector<int> charge_all;
    std::vector<int> time_all;
    std::vector<int> strip_number;
    std::vector<int> numberofstrips_percluster;
    std::vector<short int> strip_times;
    std::vector<int> strip_charges;
    std::vector<float> R_mon;
    std::vector<float> z_mon;
    std::vector<float> x_mon;
    std::vector<float> y_mon;
    std::vector<int> stationPhi_ASide_eta1_ontrack;
    std::vector<int> stationPhi_ASide_eta2_ontrack;
    std::vector<int> stationPhi_CSide_eta1_ontrack;
    std::vector<int> stationPhi_CSide_eta2_ontrack;
    std::vector<int> sector_ASide_eta2_ontrack;
    std::vector<int> sector_CSide_eta1_ontrack;
    std::vector<int> sector_ASide_eta1_ontrack;
    std::vector<int> sector_CSide_eta2_ontrack;
    std::vector<int> sector_lb_ASide_eta1_ontrack;
    std::vector<int> sector_lb_ASide_eta2_ontrack;
    std::vector<int> sector_lb_CSide_eta1_ontrack;
    std::vector<int> sector_lb_CSide_eta2_ontrack;
    std::vector<int> stationPhi_ASide_eta1;
    std::vector<int> stationPhi_ASide_eta2;
    std::vector<int> stationPhi_CSide_eta1;
    std::vector<int> stationPhi_CSide_eta2;
    std::vector<int> sector_CSide_eta2;
    std::vector<int> sector_ASide_eta2;
    std::vector<int> sector_CSide_eta1;
    std::vector<int> sector_ASide_eta1;
    std::vector<int> sector_lb_ASide_eta1;
    std::vector<int> sector_lb_ASide_eta2;
    std::vector<int> sector_lb_CSide_eta1;
    std::vector<int> sector_lb_CSide_eta2;

  };

  struct sTGCSummaryHistogramStruct {
    
    std::vector<int> strip_number;
    std::vector<int> sector_strip;
    std::vector<int> charge;
    std::vector<short int> strip_times;
    std::vector<int> strip_charges;
    std::vector<float> x_ontrack;
    std::vector<float> y_ontrack;
    std::vector<float> residuals;
  };

}

/////////////////////////////////////////////////////////////////////////////
// *********************************************************************
// Public Methods
// ********************************************************************* 

StgcRawDataMonAlg::StgcRawDataMonAlg( const std::string& name, ISvcLocator* pSvcLocator )
  :AthMonitorAlgorithm(name,pSvcLocator)
   //m_muonSelectionTool("CP::MuonSelectionTool/MuonSelectionTool")
   //m_sTGCContainerKey("STGC_Measurements")
{
  //Declare the property 
  //declareProperty("sTGCPrepDataContainerName",m_sTGCContainerKey);
}

/*---------------------------------------------------------*/
StatusCode StgcRawDataMonAlg::initialize()
/*---------------------------------------------------------*/
{   
  // init message stream
  ATH_MSG_DEBUG("initialize sTGCRawDataMonAlg" );
  ATH_MSG_DEBUG("******************" );
  ATH_MSG_DEBUG("doSTGCESD: " << m_doSTGCESD );
  ATH_MSG_DEBUG("******************" );
  
  ATH_CHECK(AthMonitorAlgorithm::initialize());
  ATH_CHECK(m_DetectorManagerKey.initialize());
  ATH_CHECK(m_idHelperSvc.retrieve());

  ATH_MSG_INFO(" Found the MuonIdHelperSvc " );
  ATH_CHECK(m_muonKey.initialize());
  ATH_CHECK(m_sTGCContainerKey.initialize() );
  
  ATH_MSG_DEBUG(" end of initialize " );
  ATH_MSG_INFO("sTGCRawDataMonAlg initialization DONE " );

  return StatusCode::SUCCESS;
} 

StatusCode StgcRawDataMonAlg::fillHistograms(const EventContext& ctx) const
{  
  int lumiblock = -1;

  lumiblock = GetEventInfo(ctx)->lumiBlock();

  ATH_MSG_INFO("sTGCRawDataMonAlg::sTGC RawData Monitoring Histograms being filled" );

  SG::ReadHandle<Muon::sTgcPrepDataContainer> sTGC_container(m_sTGCContainerKey,ctx);

  ATH_MSG_INFO("****** sTGCContainer->size() : " << sTGC_container->size());

  if (m_doSTGCESD) {
    sTGCOverviewHistogramStruct overviewPlots;
    sTGCSummaryHistogramStruct summaryPlots[2][2][8][2][2][4];

  //loop in sTGCPrepDataContainer                                                                             
         
  for(const Muon::sTgcPrepDataCollection* coll : *sTGC_container) {
    for (const Muon::sTgcPrepData* prd : *coll){
      ATH_CHECK(fillsTGCOverviewVects(prd, overviewPlots));
      ATH_CHECK(fillsTGCSummaryVects(prd, summaryPlots));
      ATH_CHECK(fillsTGCHistograms(prd));
    }
  }

  if (m_do_sTgc_overview) fillsTGCOverviewHistograms(overviewPlots,lumiblock);

  ATH_CHECK( fillsTGCSummaryHistograms(summaryPlots) );

  const xAOD::TrackParticleContainer* meTPContainer = nullptr;
  ATH_CHECK(evtStore()->retrieve(meTPContainer,"ExtrapolatedMuonTrackParticles" ));
  clusterFromTrack(meTPContainer,lumiblock);
  }

 return StatusCode::SUCCESS;

}

StatusCode StgcRawDataMonAlg::fillsTGCOverviewVects( const Muon::sTgcPrepData* prd, sTGCOverviewHistogramStruct& vects ) const {

  Identifier Id = prd->identify();
  const std::vector<Identifier>& stripIds = prd->rdoList();
  unsigned int nStrips = stripIds.size();  // number of strips in this cluster (cluster size)               
  const std::vector<uint16_t>& stripNumbers=prd->stripNumbers();
 
  std::string stName   = m_idHelperSvc->stgcIdHelper().stationNameString(m_idHelperSvc->stgcIdHelper().stationName(Id));
  int gas_gap          = m_idHelperSvc->stgcIdHelper().gasGap(Id);
  int stationNumber    = m_idHelperSvc->stgcIdHelper().stationName(Id);
  int stationEta       = m_idHelperSvc->stgcIdHelper().stationEta(Id);
  int stationPhi       = m_idHelperSvc->stgcIdHelper().stationPhi(Id);
  int multiplet        = m_idHelperSvc->stgcIdHelper().multilayer(Id);
  int channel          = m_idHelperSvc->stgcIdHelper().channel(Id);
   
  // Returns the charge (number of electrons) converted in fC
  int charge=prd->charge()*conversion_charge;
  // Returns the time (in ns)
  int drift_time=prd->time();
  // Returns the microTPC angle (radians converted in degrees)
  //float mu_TPC_angle=prd->angle()*toDeg;
  // Returns the microTPC chisq Prob.
  //float mu_TPC_chi2=prd->chisqProb();
  //const std::vector<short int>& strip_times=prd->stripTimes();
  //const std::vector<int>& strip_charges=prd->stripCharges();

  Amg::Vector3D pos    = prd->globalPosition();
  
  float R=std::hypot(pos.x(),pos.y());

  int PCB=get_PCB_from_channel(channel);

  //MM gaps are back to back, so the direction of the drift (time) is different for the even and odd gaps -> flip for the even gaps

  //if (gas_gap % 2 == 0) { mu_TPC_angle = -mu_TPC_angle; } 
  
  vects.charge_all.push_back(charge);
  vects.time_all.push_back(drift_time);
  vects.numberofstrips_percluster.push_back(nStrips);
  //vects.strip_times.insert(strip_times);
  //vects.strip_charges.insert(strip_charges);
  vects.x_mon.push_back(pos.x());
  vects.y_mon.push_back(pos.y());
  vects.z_mon.push_back(pos.z());
  vects.R_mon.push_back(R);

    

  //MMS and MML phi sectors
  int phisec=0;
  if (stationNumber%2 == 0) phisec=1;
    
  //16 phi sectors, 8 stationPhi times 2 stName, MMS and MML
  int sectorPhi=get_sectorPhi_from_stationPhi_stName(stationPhi,stName);  
  
  //Occupancy plots with PCB granularity further divided for each eta sector: -2, -1, 1, 2
  
  //Filling Vectors for stationEta=-2
  if (stationEta==-2){
    vects.sector_CSide_eta2.push_back(get_bin_for_occ_CSide_pcb_eta2_hist(stationEta,multiplet,gas_gap,PCB));
    vects.stationPhi_CSide_eta2.push_back(sectorPhi);
    vects.sector_lb_CSide_eta2.push_back(get_bin_for_occ_lb_CSide_pcb_eta2_hist(stationEta,multiplet,gas_gap,PCB,phisec));
  }
  //Filling Vectors for stationEta=-1
  else if (stationEta==-1){
    vects.sector_CSide_eta1.push_back(get_bin_for_occ_CSide_pcb_eta1_hist(stationEta,multiplet,gas_gap,PCB));
    vects.stationPhi_CSide_eta1.push_back(sectorPhi);
    vects.sector_lb_CSide_eta1.push_back(get_bin_for_occ_lb_CSide_pcb_eta1_hist(stationEta,multiplet,gas_gap,PCB,phisec));
  }
  //Filling Vectors for stationEta=1
  else if (stationEta==1){
    vects.sector_ASide_eta1.push_back(get_bin_for_occ_ASide_pcb_eta1_hist(stationEta,multiplet,gas_gap,PCB));
    vects.stationPhi_ASide_eta1.push_back(sectorPhi);
    vects.sector_lb_ASide_eta1.push_back(get_bin_for_occ_lb_ASide_pcb_eta1_hist(stationEta,multiplet,gas_gap,PCB,phisec));
  }
  //Filling Vectors for stationEta=2
  else {
    vects.sector_ASide_eta2.push_back(get_bin_for_occ_ASide_pcb_eta2_hist(stationEta,multiplet,gas_gap,PCB));
    vects.stationPhi_ASide_eta2.push_back(sectorPhi);
    vects.sector_lb_ASide_eta2.push_back(get_bin_for_occ_lb_ASide_pcb_eta2_hist(stationEta,multiplet,gas_gap,PCB,phisec));
  }

  //loop on each strip                                                                                       
  
  int sIdx = 0; // index-counter for the vector of Id's                                
  for (const Identifier& id : stripIds){    
    
    std::string stName_strip   = m_idHelperSvc->stgcIdHelper().stationNameString(m_idHelperSvc->stgcIdHelper().stationName(id));
    int stationEta_strip       = m_idHelperSvc->stgcIdHelper().stationEta(id);

    vects.statEta_strip.push_back(stationEta_strip);
    if(stripNumbers.size()!=0){
      vects.strip_number.push_back(stripNumbers[sIdx]);
    }
    ++sIdx;
  }
  return StatusCode::SUCCESS;
}


void StgcRawDataMonAlg::fillsTGCOverviewHistograms( const sTGCOverviewHistogramStruct& vects, int lb ) const {

  auto charge_all = Monitored::Collection("charge_all", vects.charge_all);
  auto numberofstrips_percluster = Monitored::Collection("numberofstrips_percluster", vects.numberofstrips_percluster);
  fill("StgcMonitor",charge_all,numberofstrips_percluster);

  auto time_all = Monitored::Collection("time_all", vects.time_all);
  auto strip_times = Monitored::Collection("strip_times", vects.strip_times);
  auto strip_charges = Monitored::Collection("strip_charges", vects.strip_charges);
  auto strip_number = Monitored::Collection("strip_number", vects.strip_number);
  auto statEta_strip = Monitored::Collection("statEta_strip", vects.statEta_strip);

  fill("StgcMonitor",time_all,strip_times,strip_charges,strip_number,statEta_strip);

  auto x_mon = Monitored::Collection("x_mon", vects.x_mon);
  auto y_mon = Monitored::Collection("y_mon", vects.y_mon);
  auto z_mon = Monitored::Collection("z_mon", vects.z_mon);
  auto R_mon = Monitored::Collection("R_mon", vects.R_mon);

  fill("StgcMonitor",x_mon,y_mon,z_mon,R_mon);
    
  auto lb_mon = Monitored::Scalar<int>("lb_mon", lb);
    
  auto sector_lb_CSide_eta2 = Monitored::Collection("sector_lb_CSide_eta2",vects.sector_lb_CSide_eta2);
  auto sector_lb_CSide_eta1 = Monitored::Collection("sector_lb_CSide_eta1",vects.sector_lb_CSide_eta1);
  auto sector_lb_ASide_eta2 = Monitored::Collection("sector_lb_ASide_eta2",vects.sector_lb_ASide_eta2);
  auto sector_lb_ASide_eta1 = Monitored::Collection("sector_lb_ASide_eta1",vects.sector_lb_ASide_eta1);
    
  fill("StgcMonitor",lb_mon,sector_lb_CSide_eta2,sector_lb_CSide_eta1,sector_lb_ASide_eta1,sector_lb_ASide_eta2);
    
  auto sector_CSide_eta2 = Monitored::Collection("sector_CSide_eta2",vects.sector_CSide_eta2);
  auto sector_CSide_eta1 = Monitored::Collection("sector_CSide_eta1",vects.sector_CSide_eta1);
  auto sector_ASide_eta1 = Monitored::Collection("sector_ASide_eta1",vects.sector_ASide_eta1);
  auto sector_ASide_eta2 = Monitored::Collection("sector_ASide_eta2",vects.sector_ASide_eta2);
  auto stationPhi_CSide_eta1 = Monitored::Collection("stationPhi_CSide_eta1",vects.stationPhi_CSide_eta1);
  auto stationPhi_CSide_eta2 = Monitored::Collection("stationPhi_CSide_eta2",vects.stationPhi_CSide_eta2);
  auto stationPhi_ASide_eta1 = Monitored::Collection("stationPhi_ASide_eta1",vects.stationPhi_ASide_eta1);
  auto stationPhi_ASide_eta2 = Monitored::Collection("stationPhi_ASide_eta2",vects.stationPhi_ASide_eta2);
  
  fill("StgcMonitor",sector_CSide_eta1,sector_CSide_eta2,sector_ASide_eta1,sector_ASide_eta2,stationPhi_CSide_eta1,stationPhi_CSide_eta2,stationPhi_ASide_eta1,stationPhi_ASide_eta2);
    
    

}

StatusCode StgcRawDataMonAlg::fillsTGCSummaryVects( const Muon::sTgcPrepData* prd, sTGCSummaryHistogramStruct (&vects)[2][2][8][2][2][4]) const{

  Identifier Id = prd->identify();
  const std::vector<Identifier>& stripIds = prd->rdoList();

  std::string stName   = m_idHelperSvc->stgcIdHelper().stationNameString(m_idHelperSvc->stgcIdHelper().stationName(Id));
  int thisStationNumber    = m_idHelperSvc->stgcIdHelper().stationName(Id);
  int thisStationEta       = m_idHelperSvc->stgcIdHelper().stationEta(Id);
  int thisStationPhi       = m_idHelperSvc->stgcIdHelper().stationPhi(Id);
  int thisMultiplet        = m_idHelperSvc->stgcIdHelper().multilayer(Id);
  int thisGasgap          = m_idHelperSvc->stgcIdHelper().gasGap(Id);
  int thisCharge=prd->charge()*conversion_charge;

  //float thisMu_TPC_angle=prd->angle()*toDeg;
    
  //if ( thisGasgap % 2 == 0 ) { thisMu_TPC_angle = - thisMu_TPC_angle; }
    
  //MMS and MML phi sectors
  int phisec=0;
  if (thisStationNumber%2 == 0) phisec=1;
  
  //CSide and ASide
  int iside=0;
  if(thisStationEta>0) iside=1;
  
  //2 eta sectors depending on Eta=+-1 (0) and +-2 (1)
  int sectorEta=get_sectorEta_from_stationEta(thisStationEta);

  auto& Vectors = vects[iside][phisec][thisStationPhi-1][sectorEta][thisMultiplet-1][thisGasgap-1];
  
  //Vectors.mu_TPC_angle.push_back(thisMu_TPC_angle);
  Vectors.charge.push_back(thisCharge);
  
  //loop on strips
  int sIdx = 0;
  const std::vector<uint16_t>& stripNumbers=prd->stripNumbers();
  
  for ( const Identifier& id : stripIds){
    
    int stationEta       = m_idHelperSvc->stgcIdHelper().stationEta(id);
    int gas_gap          = m_idHelperSvc->stgcIdHelper().gasGap(Id);
    int multiplet        = m_idHelperSvc->stgcIdHelper().multilayer(Id); 

    //    Filling Vectors for both sides, considering each strip  
    if(stripNumbers.size()!=0){
      Vectors.strip_number.push_back(stripNumbers[sIdx]);
    }
    if(iside==1)    Vectors.sector_strip.push_back(get_bin_for_occ_ASide_hist(stationEta,multiplet,gas_gap));
    if(iside==0)    Vectors.sector_strip.push_back(get_bin_for_occ_CSide_hist(stationEta,multiplet,gas_gap));
    
  }

  return StatusCode::SUCCESS;
}


StatusCode StgcRawDataMonAlg::fillsTGCSummaryHistograms( const sTGCSummaryHistogramStruct (&vects)[2][2][8][2][2][4]) const{
  
    
  for (int iside=0;iside<2;iside++){
    std::string sTGC_sideGroup = "sTGC_sideGroup"+sTGC_Side[iside];
    
    for (int isector=0;isector<2;isector++){
      for( int statPhi=0; statPhi<8; statPhi++) {
	for( int statEta=0; statEta<2; statEta++) {
	  for( int multiplet=0; multiplet<2; multiplet++) {
	    for( int gas_gap=0; gas_gap<4; gas_gap++) {
	      
	      auto& Vectors = vects[iside][isector][statPhi][statEta][multiplet][gas_gap];
                          
	      auto sector_strip=Monitored::Collection("sector_strip_"+sTGC_Side[iside]+"_"+sTGC_Sector[isector]+"_phi"+std::to_string(statPhi+1),Vectors.sector_strip);
	      auto strip_number = Monitored::Collection("strip_number_"+sTGC_Side[iside]+"_"+sTGC_Sector[isector]+"_phi"+std::to_string(statPhi+1), Vectors.strip_number);
	      
	      fill(sTGC_sideGroup,strip_number,sector_strip);

	      auto charge_perLayer = Monitored::Collection("charge_"+sTGC_Side[iside]+"_sector_"+sTGC_Sector[isector]+"_phi"+std::to_string(statPhi+1)+"_stationEta"+EtaSector[statEta]+"_multiplet"+std::to_string(multiplet+1)+"_gas_gap"+std::to_string(gas_gap+1), Vectors.charge);
	      auto strip_times = Monitored::Collection("strip_times_"+sTGC_Side[iside]+"_sector_"+sTGC_Sector[isector]+"_phi"+std::to_string(statPhi+1)+"_stationEta"+EtaSector[statEta]+"_multiplet"+std::to_string(multiplet+1)+"_gas_gap"+std::to_string(gas_gap+1),Vectors.strip_charges);

	      fill(sTGC_sideGroup,charge_perLayer,strip_times);
	      
	    }
	  }
	}
      }
    }
  }
 
  return StatusCode::SUCCESS;
    
}

StatusCode StgcRawDataMonAlg::fillsTGCHistograms( const Muon::sTgcPrepData* ) const{

  return StatusCode::SUCCESS;
}

                                                                                                           
      
void StgcRawDataMonAlg::clusterFromTrack(const xAOD::TrackParticleContainer*  muonContainer, int lb) const{

  sTGCSummaryHistogramStruct summaryPlots[2][2][4];
  sTGCSummaryHistogramStruct summaryPlots_full[2][16][2][2][4];
  sTGCOverviewHistogramStruct overviewPlots;

  int nmu=0;

  for (const xAOD::TrackParticle* meTP  : *muonContainer){

    if (meTP) {
      nmu++;
      auto eta_trk = Monitored::Scalar<float>("eta_trk", meTP->eta());
      auto phi_trk = Monitored::Scalar<float>("phi_trk", meTP->phi());

      // retrieve the original track                                                       
      const Trk::Track* meTrack = meTP->track();
      if (meTrack) {
        // get the vector of measurements on track                                                           

	const DataVector<const Trk::MeasurementBase>* meas = meTrack->measurementsOnTrack();

	
	for(const Trk::MeasurementBase* it: *meas){
	
	  const Trk::RIO_OnTrack* rot = dynamic_cast<const Trk::RIO_OnTrack*>(it);
	  
	  if (rot) {
	    Identifier rot_id = rot->identify();
	    if (m_idHelperSvc->issTgc(rot_id)) {
	      const Muon::sTgcClusterOnTrack* cluster = dynamic_cast<const Muon::sTgcClusterOnTrack*>(rot);
	      
	      if (cluster) {
		
		std::string stName   = m_idHelperSvc->stgcIdHelper().stationNameString(m_idHelperSvc->stgcIdHelper().stationName(rot_id));
		int stNumber    = m_idHelperSvc->stgcIdHelper().stationName(rot_id);
		int stEta= m_idHelperSvc->stgcIdHelper().stationEta(rot_id);
		int stPhi= m_idHelperSvc->stgcIdHelper().stationPhi(rot_id);
		int multi = m_idHelperSvc->stgcIdHelper().multilayer(rot_id);
		int gap=  m_idHelperSvc->stgcIdHelper().gasGap(rot_id);
		int ch=  m_idHelperSvc->stgcIdHelper().channel(rot_id);
                        
		//MMS and MML phi sectors                                                                    
		int phisec=0;
		if (stNumber%2 == 0) phisec=1;
		
		int sectorPhi=get_sectorPhi_from_stationPhi_stName(stPhi,stName);
		
		int PCB=get_PCB_from_channel(ch);
		
		auto& vects=overviewPlots;
		
		//Occupancy plots with PCB granularity further divided for each eta sector: -2, -1, 1, 2  
		//Filling Vectors for stationEta=-1 - cluster on track
		if (stEta==-1){
		  vects.stationPhi_CSide_eta1_ontrack.push_back(sectorPhi);
		  vects.sector_CSide_eta1_ontrack.push_back(get_bin_for_occ_CSide_pcb_eta1_hist(stEta,multi,gap,PCB));
		  vects.sector_lb_CSide_eta1_ontrack.push_back(get_bin_for_occ_lb_CSide_pcb_eta1_hist(stEta,multi,gap,PCB,phisec));
		}
		//Filling Vectors for stationEta=-2 - cluster on track 
		else if (stEta==-2){
		  vects.stationPhi_CSide_eta2_ontrack.push_back(sectorPhi);
		  vects.sector_CSide_eta2_ontrack.push_back(get_bin_for_occ_CSide_pcb_eta2_hist(stEta,multi,gap,PCB));
		  vects.sector_lb_CSide_eta2_ontrack.push_back(get_bin_for_occ_lb_CSide_pcb_eta2_hist(stEta,multi,gap,PCB,phisec));
		}
		//Filling Vectors for stationEta=1 - cluster on track 
		else if (stEta==1){
		  vects.stationPhi_ASide_eta1_ontrack.push_back(sectorPhi);
		  vects.sector_ASide_eta1_ontrack.push_back(get_bin_for_occ_ASide_pcb_eta1_hist(stEta,multi,gap,PCB));
		  vects.sector_lb_ASide_eta1_ontrack.push_back(get_bin_for_occ_lb_ASide_pcb_eta1_hist(stEta,multi,gap,PCB,phisec));
		}
		//Filling Vectors for stationEta=2 - cluster on track 
		else {
		  vects.stationPhi_ASide_eta2_ontrack.push_back(sectorPhi);
		  vects.sector_ASide_eta2_ontrack.push_back(get_bin_for_occ_ASide_pcb_eta2_hist(stEta,multi,gap,PCB));
		  vects.sector_lb_ASide_eta2_ontrack.push_back(get_bin_for_occ_lb_ASide_pcb_eta2_hist(stEta,multi,gap,PCB,phisec));
		  
		  }

		float x =		cluster->localParameters()[Trk::loc1] ;

		for (const Trk::TrackStateOnSurface* trkState: *meTrack->trackStateOnSurfaces()) {

		  if(!(trkState)) continue;
		  Identifier surfaceId = (trkState)->surface().associatedDetectorElementIdentifier();
		  if(!m_idHelperSvc->issTgc(surfaceId)) continue;

		  int trk_stEta= m_idHelperSvc->stgcIdHelper().stationEta(surfaceId);
		  int trk_stPhi= m_idHelperSvc->stgcIdHelper().stationPhi(surfaceId);
		  int trk_multi = m_idHelperSvc->stgcIdHelper().multilayer(surfaceId);
		  int trk_gap=  m_idHelperSvc->stgcIdHelper().gasGap(surfaceId);
		  if(  trk_stPhi==stPhi  and trk_stEta==stEta and trk_multi==multi and trk_gap==gap ){
		    double x_trk = trkState->trackParameters()->parameters()[Trk::loc1];
		    double y_trk = trkState->trackParameters()->parameters()[Trk::locY];
		    
		    int stPhi16=0;
		    if (stName=="L")   stPhi16=2*stPhi-1;
		    
		    if (stName=="S")  stPhi16=2*stPhi;

		    int iside=0;
		    if(stEta>0) iside=1;

		    float stereo_angle=		      0.02618;
		    //		    float stereo_correction=stereo_angle*y_trk;
		    float stereo_correction=sin(stereo_angle)*y_trk;
		    if(multi==1 && gap<3) stereo_correction=0;
		    if(multi==2 && gap>2) stereo_correction=0;
		    if(multi==1 && gap==3 ) stereo_correction*=-1;
		    if(multi==2 && gap==1 ) stereo_correction*=-1;
		    if(multi==1 && gap<3) stereo_angle=0;
		    if(multi==2 && gap>2) stereo_angle=0;
		    float res_stereo = (x - x_trk)*cos(stereo_angle) - stereo_correction;
		    auto residual_mon = Monitored::Scalar<float>("residual", res_stereo);
		    auto stPhi_mon = Monitored::Scalar<float>("stPhi_mon",stPhi16);
		    fill("sTGCMonitor",residual_mon, eta_trk, phi_trk, stPhi_mon);
		    int abs_stEta= get_sectorEta_from_stationEta(stEta);
		    auto& vectors = summaryPlots_full[iside][stPhi16-1][abs_stEta-1][multi-1][gap-1];
		    vectors.residuals.push_back(res_stereo);
		  }
		}
	      } //if cluster
	    } //isMM
	  } // if rot
	} // loop on meas
	
	for (int iside=0;iside<2;iside++){
	std::string sTGC_sideGroup = "sTGC_sideGroup"+sTGC_Side[iside];
	  for( int statPhi=0; statPhi<16; statPhi++) {
	    for( int statEta=0; statEta<2; statEta++) {
	      for( int multiplet=0; multiplet<2; multiplet++) {
		for( int gas_gap=0; gas_gap<4; gas_gap++) {	
		  auto& vects=summaryPlots_full[iside][statPhi][statEta][multiplet][gas_gap];;
		  auto residuals_gap = Monitored::Collection("residuals_"+sTGC_Side[iside]+"_phi"+std::to_string(statPhi+1)+"_stationEta"+EtaSector[statEta]+"_multiplet"+std::to_string(multiplet+1)+"_gas_gap"+std::to_string(gas_gap+1),vects.residuals);
		  std::string sTGC_GapGroup = "sTGC_GapGroup"+std::to_string(gas_gap+1);

		  fill(sTGC_sideGroup,residuals_gap);
		}
	      }}}}




	for (const Trk::TrackStateOnSurface* trkState: *meTrack->trackStateOnSurfaces()) {
	  
	  if(!(trkState)) continue;
	  Identifier surfaceId = (trkState)->surface().associatedDetectorElementIdentifier();
	  if(!m_idHelperSvc->issTgc(surfaceId)) continue;
	  
	  const Amg::Vector3D& pos    = (trkState)->trackParameters()->position();
	  int stEta= m_idHelperSvc->stgcIdHelper().stationEta(surfaceId);
	  int multi = m_idHelperSvc->stgcIdHelper().multilayer(surfaceId);
	  int gap=  m_idHelperSvc->stgcIdHelper().gasGap(surfaceId);

	  //CSide and ASide                                                                                  
	  int iside=0;
	  if(stEta>0) iside=1;

	  auto& Vectors = summaryPlots[iside][multi-1][gap-1];	  

	  //Filling x-y position vectors using the trackStateonSurface 
	  Vectors.x_ontrack.push_back(pos.x());
	  Vectors.y_ontrack.push_back(pos.y());
	    
	}
      } // if meTrack
    } // if muon
  } //loop on muonContainer

  auto& vects=overviewPlots;

  auto stationPhi_CSide_eta1_ontrack = Monitored::Collection("stationPhi_CSide_eta1_ontrack",vects.stationPhi_CSide_eta1_ontrack);
  auto stationPhi_CSide_eta2_ontrack = Monitored::Collection("stationPhi_CSide_eta2_ontrack",vects.stationPhi_CSide_eta2_ontrack);
  auto stationPhi_ASide_eta1_ontrack = Monitored::Collection("stationPhi_ASide_eta1_ontrack",vects.stationPhi_ASide_eta1_ontrack);
  auto stationPhi_ASide_eta2_ontrack = Monitored::Collection("stationPhi_ASide_eta2_ontrack",vects.stationPhi_ASide_eta2_ontrack);
  auto sector_ASide_eta1_ontrack = Monitored::Collection("sector_ASide_eta1_ontrack",vects.sector_ASide_eta1_ontrack);
  auto sector_ASide_eta2_ontrack = Monitored::Collection("sector_ASide_eta2_ontrack",vects.sector_ASide_eta2_ontrack);
  auto sector_CSide_eta2_ontrack = Monitored::Collection("sector_CSide_eta2_ontrack",vects.sector_CSide_eta2_ontrack);                                                                                         
  auto sector_CSide_eta1_ontrack = Monitored::Collection("sector_CSide_eta1_ontrack",vects.sector_CSide_eta1_ontrack);   

  auto lb_ontrack = Monitored::Scalar<int>("lb_ontrack", lb);

  auto sector_lb_CSide_eta2_ontrack = Monitored::Collection("sector_lb_CSide_eta2_ontrack",vects.sector_lb_CSide_eta2_ontrack);
  auto sector_lb_CSide_eta1_ontrack = Monitored::Collection("sector_lb_CSide_eta1_ontrack",vects.sector_lb_CSide_eta1_ontrack);
  auto sector_lb_ASide_eta2_ontrack = Monitored::Collection("sector_lb_ASide_eta2_ontrack",vects.sector_lb_ASide_eta2_ontrack);
  auto sector_lb_ASide_eta1_ontrack = Monitored::Collection("sector_lb_ASide_eta1_ontrack",vects.sector_lb_ASide_eta1_ontrack);

  fill("sTGCMonitor",stationPhi_CSide_eta1_ontrack,stationPhi_CSide_eta2_ontrack,stationPhi_ASide_eta1_ontrack,stationPhi_ASide_eta2_ontrack,sector_CSide_eta1_ontrack,sector_CSide_eta2_ontrack,sector_ASide_eta1_ontrack,sector_ASide_eta2_ontrack,sector_lb_CSide_eta2_ontrack,sector_lb_CSide_eta1_ontrack,sector_lb_ASide_eta2_ontrack,sector_lb_ASide_eta1_ontrack,lb_ontrack);
    
  for (int iside=0;iside<2;iside++){
   std::string sTGC_sideGroup = "sTGC_sideGroup"+sTGC_Side[iside];
   for( int multiplet=0; multiplet<2; multiplet++) {
     for( int gas_gap=0; gas_gap<4; gas_gap++) {
	      
       auto& Vectors = summaryPlots[iside][multiplet][gas_gap];

       auto x_ontrack = Monitored::Collection("x_"+sTGC_Side[iside]+"_multiplet"+std::to_string(multiplet+1)+"_gas_gap_"+std::to_string(gas_gap+1)+"_ontrack", Vectors.x_ontrack);
       auto y_ontrack = Monitored::Collection("y_"+sTGC_Side[iside]+"_multiplet"+std::to_string(multiplet+1)+"_gas_gap_"+std::to_string(gas_gap+1)+"_ontrack", Vectors.y_ontrack);
      
       fill(sTGC_sideGroup,x_ontrack,y_ontrack);
     }          
   }
  }
           
  
}
