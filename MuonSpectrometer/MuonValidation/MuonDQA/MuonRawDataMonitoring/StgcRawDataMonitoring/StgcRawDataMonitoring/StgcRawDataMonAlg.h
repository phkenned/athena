/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////////////////////////////
// Package : sTGCRawDataMonitoring
// Author: P. D. Kennedy
// MMAuthor:  M. Biglietti, E. Rossi (Roma Tre)
//
// DESCRIPTION:
// Subject: sTGC-->Offline Muon Data Quality
///////////////////////////////////////////////////////////////////////////////////////////

#ifndef StgcRawDataMonAlg_H
#define StgcRawDataMonAlg_H

//Core Include
#include "AthenaMonitoring/AthMonitorAlgorithm.h"
#include "AthenaMonitoringKernel/Monitored.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h" 
//Helper Includes

#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "MuonPrepRawData/MuonPrepDataContainer.h"
#include "MuonPrepRawData/sTgcPrepDataCollection.h"
#include "MuonIdHelpers/IMuonIdHelperSvc.h"
#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "AthenaMonitoring/DQAtlasReadyFilterTool.h"
#include "xAODTrigger/MuonRoIContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "MuonPrepRawData/sTgcPrepDataContainer.h"
#include "MuonPrepRawData/sTgcPrepData.h"
#include "StoreGate/ReadHandleKey.h"


namespace Muon {
  class sTGCPrepData;
  }

namespace {
  struct sTGCOverviewHistogramStruct;
  struct sTGCSummaryHistogramStruct;
}

//stl includes                                                                                              
#include <string>

class StgcRawDataMonAlg: public AthMonitorAlgorithm {
 public:

  StgcRawDataMonAlg( const std::string& name, ISvcLocator* pSvcLocator );

  //  virtual ~MMRawDataMonAlg();
  virtual ~StgcRawDataMonAlg()=default;
  virtual StatusCode initialize() override;
  virtual StatusCode fillHistograms(const EventContext& ctx) const override;
  
 private:  

  ServiceHandle<Muon::IMuonIdHelperSvc> m_idHelperSvc {this, "MuonIdHelperSvc", "Muon::MuonIdHelperSvc/MuonIdHelperSvc"};

  ToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool{this,"MuonSelectionTool","CP::MuonSelectionTool/MuonSelectionTool"};
  SG::ReadCondHandleKey<MuonGM::MuonDetectorManager> m_DetectorManagerKey {this, "DetectorManagerKey",
     "MuonDetectorManager","Key of input MuonDetectorManager condition data"};
 
  virtual StatusCode  fillsTGCOverviewVects(const Muon::sTgcPrepData*, sTGCOverviewHistogramStruct& vects) const;
  virtual void  fillsTGCOverviewHistograms(const sTGCOverviewHistogramStruct& vects,const int lb) const;
  virtual StatusCode  fillsTGCSummaryVects( const Muon::sTgcPrepData*, sTGCSummaryHistogramStruct (&vects)[2][2][8][2][2][4]) const; //[side][sector][stationPhi][stationEta][multiplet][gas_gap]
  virtual StatusCode  fillsTGCHistograms( const Muon::sTgcPrepData* ) const;                                      
  virtual StatusCode  fillsTGCSummaryHistograms( const sTGCSummaryHistogramStruct (&vects)[2][2][8][2][2][4]) const;

  void clusterFromTrack(const xAOD::TrackParticleContainer*,const int lb) const;
  
  int get_PCB_from_channel(const int channel) const;
  int get_sectorPhi_from_stationPhi_stName(const int stationPhi, const std::string& stName) const;
  int get_sectorEta_from_stationEta(const int stationEta) const;

  int get_bin_for_occ_CSide_hist(const int stationEta, const int multiplet, const int gas_gap) const;
  int get_bin_for_occ_ASide_hist(const int stationEta, const int multiplet, const int gas_gap) const;
  int get_bin_for_occ_CSide_pcb_eta2_hist(const int stationEta, const int multiplet, const int gas_gap, const int PCB) const;
  int get_bin_for_occ_CSide_pcb_eta1_hist(const int stationEta, const int multiplet, const int gas_gap, const int PCB) const;
  int get_bin_for_occ_ASide_pcb_eta2_hist(const int stationEta, const int multiplet, const int gas_gap, const int PCB) const;
  int get_bin_for_occ_ASide_pcb_eta1_hist(const int stationEta, const int multiplet, const int gas_gap, const int PCB) const;
  int get_bin_for_occ_lb_CSide_pcb_eta2_hist(const int stationEta, const int multiplet, const int gas_gap, const int PCB,const int isector) const;
  int get_bin_for_occ_lb_CSide_pcb_eta1_hist(const int stationEta, const int multiplet, const int gas_gap, const int PCB,int isector) const;
  int get_bin_for_occ_lb_ASide_pcb_eta1_hist(const int stationEta, const int multiplet, const int gas_gap, const int PCB,int isector) const;
  int get_bin_for_occ_lb_ASide_pcb_eta2_hist(const int stationEta, const int multiplet, const int gas_gap, const int PCB, const int isector) const;

  SG::ReadHandleKey<Muon::sTgcPrepDataContainer> m_sTGCContainerKey{this,"sTGCPrepDataContainerName","STGC_Measurements"};
  SG::ReadHandleKey<xAOD::MuonContainer> m_muonKey{this,"MuonKey","Muons","muons"};

  Gaudi::Property<bool> m_doSTGCESD{this,"DoSTGCESD",true};
  Gaudi::Property<bool> m_do_sTgc_overview{this,"do_sTgc_overview",true};
  
   
};    
#endif
